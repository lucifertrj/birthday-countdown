import tkinter as tk
from PIL import Image,ImageTk

class Timer:
    def __init__(self) -> None:
        pass

    def main(self):
        self.root = tk.Tk()
        self.root.geometry('490x441+300+120')
        self.root.title("Happy Birthday Legends")
        self.start = 10
        self.label = tk.Label(self.root,text=10,font=("arial",18,"bold"),bg="black",fg="white")
        self.label.pack()
        self.label.after(ms=1000,func=self.delay)
        img = Image.open('oct10.png')
        img = img.convert("RGBA")
        img = img.resize((500,480), Image.ANTIALIAS)
        bg= ImageTk.PhotoImage(img)
        canvas= tk.Canvas(self.root)
        canvas.pack(expand=True, fill= "both")
        canvas.create_image(0,80,image=bg,anchor="nw")
        self.root.mainloop()


    def delay(self):
        self.start -=1
        if self.start == 0:
            self.label.config(text="Happy birthday Sakata Gintoki")
            naruto = tk.Label(text="Happy Birthday Naruto Uzumaki",font=("arial",18,"bold"),bg="white").place(x=60,y=35)
            yu = tk.Label(text="Happy Birthday Nishinoya Yu",font=("arial",18,"bold"),bg="black",fg="white").place(x=70,y=70)
            return True
        self.label.config(text=self.start)
        self.label.after(ms=1000,func=self.delay)

timing = Timer()
timing.main()
